# DigFish
Addon to Ashita that logs the results of chocobo digging and fishing.  

## Intended and Suggested Use
* For use with Ashita through which the addon should be loaded.  
* This addon has only ever been tested against the CatseyeXI FFXI server.  
* While not necessary, it is suggested to have this addon loaded only when performing fishing or digging tasks.   
* This addon writes to a log. It does not manage the log for you. Consider deleting the log every once and a while.   

## Example Output
The file that is a result of running this script and performing digging and fishing tasks looks like the following.   

```
1703606567,digging,Pinch of dried marjoram,02:22,Darksday,Waning Crescent,31,Wind,Bhaflau Thickets
1703606568,digging,Colibri feather,02:22,Darksday,Waning Crescent,31,Wind,Bhaflau Thickets
1703606570,digging,Colibri feather,02:23,Darksday,Waning Crescent,31,Wind,Bhaflau Thickets
1703606571,digging,Pebble,02:24,Darksday,Waning Crescent,31,Wind,Bhaflau Thickets
1703606577,digging,Pinch of dried marjoram,02:26,Darksday,Waning Crescent,31,Wind,Bhaflau Thickets
```

Each row represents a new item that has been collected.   
Each row comma delimited.  
The fields are, in order, as follows:
* Unix epoch. The timestamp of the time when the item was collected.
* Event type. This can be "digging" or "fishing".
* Chat log of what was collected. This will contain the name of the item that was collected.
* VanaTime. The timestamp of the time, in-game, when the item was collected.
* VanaDay. The day of the week, in-game, when the item was collected.
* Moon Phase. The moon phase, in-game, when the item was collected.
* Moon Phase Percent. The percent of the moon phase, in-game, when the item was collected. 
* Weather. The current in-game weather at the time of item collection.
* Location. The in-game location of where the player was at the time of item collection.

## How It Works
The script works by parsing the chat log in real time.  
At new chat records are passed to the chat log, DigFish looks for a couple key patterns that uniquely identify a fishing or digging result.  
If a fishing or digging result is suspected, pattern matching is performed to identify specifically which type of activity is being performed and the name of the collected item. 
When a collected item is positively identified a new record is written to the DigFish log.  

## The Product
What is produced as a result of this addon is a directory wherin fishing and digging results have been logged.  
The expected location for this directory is here--  
```C:\catseyexi\catseyexi-client\Ashita\chatlogs\digfish```  
This directory assumes the following.
* You are playing on Windows.
* You are playing on the Catseyexi server.
* The Catseyexi client installation already produced this path, save for the 'digfish' directory at the end.
* The digfish directory will be created as soon as the first record is to be written. 

The logs are named based on the active player and the current, real-world, local, timestamp.  
For instance, if I am on my character named 'potato' and it is currently 31 December 2023 at 11.59 when I write a record, the record will be stored in the following file.  
```potato_2023.12.31.log```    
Now lets say the next record comes in two minutes later at 01 January 2024 at 00.01. This record will be stored in a newly created file.  
```potato_2024.01.01.log```  

## Deployment and Installation
The entire ```digfish\`` directory may be copied into the following directory.

```
digfish\
+
C:\catseyexi\catseyexi-client\Ashita\addons\
=
C:\catseyexi\catseyexi-client\Ashita\addons\digfish
```

This makes some assupmtions about your environment (i.e. You are using the following: Windows, Ashita, Catseyexi). If this is your case, it should work just like any other addon. 
