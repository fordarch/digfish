local data = {};

data.Constants = require('constants');


data.GetWeather = function()
    local pointer = ashita.memory.read_uint32(gState.pWeather + 0x02);
    return ashita.memory.read_uint8(pointer + 0);
end


data.ResolveString = function(table, value)
    if (table[value + 1] == nil) then
        return 'unknown';
    else
        return table[value + 1];
    end
end


data.GetTimestamp = function()
    local pointer = ashita.memory.read_uint32(gState.pVanaTime + 0x34);
    local rawTime = ashita.memory.read_uint32(pointer + 0x0C) + 92514960;
    local timestamp = {};

    timestamp.day = math.floor(rawTime / 3456);
    timestamp.hour = math.floor(rawTime / 144) % 24;
    timestamp.minute = math.floor((rawTime % 144) / 2.4);
    
    return timestamp;
end


return data;