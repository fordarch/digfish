--[[
* DigFish
* Contact: n/a
*
* This file is designed to work as an addon loaded by Ashita Addons.
* Ashita link: <https://www.ashitaxi.com/>
*
* DigFish is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* DigFish is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with DigFish. If not, see <https://www.gnu.org/licenses/>.
*
* 
* Credit to projects 
* 'LuAshitacast' <https://github.com/ThornyFFXI/LuAshitacast>
* and 
* 'chatmon' <https://ashitaxi.com/>
* Because these projects were open for reading and learning, the initial 
* Lua scripting and API interface to Ashita for DigFish was made possible.
--]]

addon.name      = 'digfish';
addon.author    = 'bryan';
addon.version   = '1.0';
addon.desc      = 'Logs the results of digging and fishing.';
addon.link      = '';


require('common')
gData = require('data');
gState = require('state')
local chat = require('chat');
local currentZoneName = 'Unknown'
local logs = T{
    name = nil,
};


--[[
* Checks if player 0 (you) are riding a mount. 
* @return (success) {string} - String indicating a mounted status. e.g. {"MOUNTED"}
* @return (failure) {string} - String indicating a non-mounted status. 
*   (e.g. "nil" => status could not be determined. "NOTMOUNTED" => non of the player's status effects indicated they are mounted).
--]]
local function check_if_mounted()
    local player_buffs = AshitaCore:GetMemoryManager():GetPlayer(0):GetBuffs()

    if (player_buffs == nil) then
        return 'nil';
    end

    if (#player_buffs <= 0) then
        return 'empty list';
    end

    for i = 0, #player_buffs do
        if (player_buffs[i] == 252) then
            return "MOUNTED";
        end
    end

    return 'NOTMOUNTED'
end


--[[
* Clean string and return matching substring pattern.
* @param {string} str - The string to clean.
* @return (success) {string, string} - The cleaned string and string indicating the type of match (e.g. fishing, digging). e.g. {"The matched string", "digging"}
* @return (failure) {string} - Empty string ('') indicating we have no match in the provided string (e.g. not a fishing or digging string).
--]]
local function clean_str(str)
    ------------------------------------------------------------------------------
    -- Assumptions made in this method
    ------------------------------------------------------------------------------
    -- ASSUMPTION: If you "Obtained: something" and you are mounted, you are digging.
    -- ASSUMPTION: If you "caught a something", you are fishing.
    
    ------------------------------------------------------------------------------
    -- The following lines parse and remove auto-translate and 'translate tags'.
    -- At this time, they have not been seen as necessary. (Dec 2023)
    ------------------------------------------------------------------------------
    --str = AshitaCore:GetChatManager():ParseAutoTranslate(str, true);
    --str = str:strip_translate(true);
    ------------------------------------------------------------------------------
    
    str = str:strip_colors();

    -- These are the types of chat log messages we care about. 
    -- "Obtained" => A digging result.
    -- "caught" => A fishing result. 
    if (not string.find(str, "Obtained") and not string.find(str, "caught")) then 
        return '';
    end

    local matched_string = ''
    local match_type = ''

    matched_string = string.match(str, "Obtained: ([a-zA-Z0-9 ]+)")
    
    if (matched_string == nil) then 
        matched_string = string.match(str, "caught a ([a-zA-Z0-9 ]+)")
    end
    
    if (matched_string == nil) then 
        -- This means something was 'Obtained' or 'caught', but we are unable to identify the record.
        -- This should never happen. Would make a good record for an error log if we had one. 
        return '', '';
    end 

    local mount_status = check_if_mounted()

    -- No need to check what we matched on because riding a mount and fishing 
    -- are mutually exclusive activities and always will be.
    if (mount_status == "MOUNTED") then
        match_type = 'digging'
    else 
        match_type = 'fishing'
    end

    -- Replace mid-linebreaks.
    matched_string = matched_string:gsub(string.char(0x07), '\n')

    -- Strip commas from matched string. Final product logged is a comma-delimited record.
    matched_string = string.gsub(matched_string, ",", "")

    -- Strip line breaks.
    matched_string = string.gsub(matched_string, "[\n\r]", "")

    return matched_string, match_type
end


-- ========================================================
--  Events Register
-- ========================================================
--[[
* event: load
* desc : Event called when the addon is being loaded.
--]]
ashita.events.register('load', 'load_cb', function ()
    local name = AshitaCore:GetMemoryManager():GetParty():GetMemberName(0);

    if (name ~= nil and name:len() > 0) then
        logs.name = name;
    end
	
	gState.Init();

    local currentZoneID = AshitaCore:GetMemoryManager():GetParty():GetMemberZone(0)
    currentZoneName = AshitaCore:GetResourceManager():GetString('zones.names', currentZoneID)
end);


--[[
* event: text_in
* desc : Event called when the addon is processing incoming text.
--]]
ashita.events.register('text_in', 'text_in_cb', function (e)
    -- Validate if the message can be logged..
    if (logs.name == nil or e.message_modified:len() == 0) then
        return;
    end

    local pm, pt
    pm, pt = clean_str(e.message_modified)

    -- Empty string returned to this variable indicates an unsuccessful text match.
    if (pm:len() == 0) then
        return;
    end

    -- Prepare the file path.
	-- Probably here (C:\catseyexi\catseyexi-client\Ashita\chatlogs\digfish\)
    local digfishdir = 'digfish'
    local d = os.date('*t');
    local n = ('%s_%.4u.%.2u.%.2u.log'):fmt(logs.name, d.year, d.month, d.day);
    local p = ('%s/%s/%s/'):fmt(AshitaCore:GetInstallPath(), 'chatlogs', digfishdir);

    -- Create the chatlogs folder if required.
    if (not ashita.fs.exists(p)) then
        ashita.fs.create_dir(p);
    end

    -- File handler, append mode
    local f = io.open(('%s/%s'):fmt(p, n), 'a');

    if (f ~= nil) then     
        local timestamp = gData.GetTimestamp();

        local vanadiel_time         = string.format("%02d:%02d",timestamp.hour, timestamp.minute);
        local unix_time             = os.time() -- Unix epoch
        local weather               = gData.ResolveString(gData.Constants.Weather, gData.GetWeather());
        local moon_phase            = gData.ResolveString(gData.Constants.MoonPhase, ((timestamp.day + 26) % 84) + 1);
        local moon_phase_percent    = gData.Constants.MoonPhasePercent[((timestamp.day + 26) % 84) + 1];
        local vanadiel_day          = gData.Constants.WeekDay[(timestamp.day % 8) + 1];

        -- Our comma-delimited record.
        local new_record = string.format("%s,%s,%s,%s,%s,%s,%s,%s,%s\n", 
            unix_time, 
            pt,
            pm, 
            vanadiel_time, 
            vanadiel_day, 
            moon_phase, 
            moon_phase_percent, 
            weather, 
            currentZoneName
        );

        f:write(new_record);
        f:close();
    end
end);

--[[
* event: packet_in
* desc : Event called when the addon is processing incoming packets.
--]]
ashita.events.register('packet_in', 'packet_in_cb', function (e)
    -- Packet: Zone Enter
    if (e.id == 0x000A) then
        local name = struct.unpack('s', e.data_modified, 0x84 + 0x01);
        if (logs.name ~= name) then
            logs.name = name;
        end

        -- On zone change, get the name of the zone
        local moghouse = struct.unpack('b', e.data, 0x80 + 1);
        if moghouse ~= 1 then
            coroutine.sleep(1)
            local currentZoneID = AshitaCore:GetMemoryManager():GetParty():GetMemberZone(0)
            currentZoneName = AshitaCore:GetResourceManager():GetString('zones.names', currentZoneID)
        end
        return;
    end

    -- Packet: Zone Exit
    if (e.id == 0x000B) then
        if (struct.unpack('b', e.data_modified, 0x04 + 0x01) == 1) then
            logs.name = nil;
        end
        return;
    end
end);